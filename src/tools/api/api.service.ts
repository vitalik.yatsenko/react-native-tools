import axios, { AxiosRequestConfig, AxiosResponse, AxiosInstance } from 'axios'
import { _getOrDef } from '../helpers'

interface IApiServiceProps {
	apiUrl: string
	getRefreshToken: () => string
	getAccessToken: () => string
	saveAccessToken: (token: string) => any
	headers?: Record<string, any>
	timeout?: number

	getNewAccessTokenRequestUrl: string
	transformAccessTokenResponse: (response: AxiosResponse) => string
}

export class ApiService {
	private apiInstance: AxiosInstance

	constructor(private readonly props: IApiServiceProps) {
		let headers = {
			'Content-Type': 'application/json',
		}
		if (props.headers) headers = Object.assign(headers, props.headers)

		this.apiInstance = axios.create({
			baseURL: this.props.apiUrl,
			headers,
			timeout: _getOrDef(props.timeout, 180000),
		})
		this.initInterceptors()
	}

	private get accessToken() {
		return this.props.getAccessToken()
	}

	private get refreshToken() {
		return this.props.getRefreshToken()
	}

	// tslint:disable-next-line: adjacent-overload-signatures
	private set accessToken(token: string) {
		this.props.saveAccessToken(token)
	}

	private initInterceptors() {
		this.apiInstance.interceptors.request.use((config: any) => {
			const token = this.accessToken
			if (token) {
				config.headers['x-authorization'] = 'Bearer ' + token
			}
			return config
		})
	}

	private async reloadAccessToken(): Promise<void> {
		const response = await this.apiInstance.post(this.props.getNewAccessTokenRequestUrl, {
			refreshToken: this.refreshToken,
		})
		this.accessToken = this.props.transformAccessTokenResponse(response)
	}

	private async request<T>(func: any): Promise<AxiosResponse<T>> {
		try {
			const response = await func()
			return (response as any) as AxiosResponse
		} catch (e) {
			if (e.response.status === 401) {
				await this.reloadAccessToken()
				return ((await func()) as any) as AxiosResponse
			}
			throw e
		}
	}

	public get<T>(url: string, params?: AxiosRequestConfig) {
		return this.request<T>(() => this.apiInstance.get<T>(url, params))
	}
	public post<T>(url: string, data: any, params?: AxiosRequestConfig) {
		return this.request<T>(() => this.apiInstance.post<T>(url, data, params))
	}
	public put<T>(url: string, data: any, params?: AxiosRequestConfig) {
		return this.request<T>(() => this.apiInstance.put<T>(url, data, params))
	}
	public patch<T>(url: string, data: any, params?: AxiosRequestConfig) {
		return this.request<T>(() => this.apiInstance.patch<T>(url, data, params))
	}
	public delete<T>(url: string, params?: AxiosRequestConfig) {
		return this.request<T>(() => this.apiInstance.delete<T>(url, params))
	}
}
