import { EventEmitter } from 'events'

const ee = new EventEmitter()

export type TEventCallback<T> = (data: T) => any
export type TEventEmitterEvents = Record<string, Record<string, any>>

export class EventsService<T> {
	private readonly eventEmitter: EventEmitter

	constructor() {
		this.eventEmitter = new EventEmitter()
	}

	public subscribeEvent<K extends Extract<keyof T, string | symbol>>(key: K, callback: TEventCallback<T[K]>) {
		this.eventEmitter.on(key as string, callback)
		return this
	}

	public emitEvent<K extends Extract<keyof T, string | symbol>>(key: K, data: T[K]) {
		this.eventEmitter.emit(key, data)
		return this
	}

	public removeListener<K extends Extract<keyof T, string>>(event: K, listener: (arg: T[K]) => void) {
		this.eventEmitter.removeListener(event, listener)
		return this
	}
}

export interface IEvents {
	NeedReloadAttr: {
		attrKeys: string
	}
	NeedReloadTimeseries: {
		timeseriesKeys: string
	}
}

const eventService = new EventsService<IEvents>()
