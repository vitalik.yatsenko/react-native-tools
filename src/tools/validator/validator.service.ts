import Validator, { Rules } from 'validatorjs'

export class ValidatorService {
	private _messages: Record<string, string> = {}

	public validate(data: any, rules: Rules, messages: Record<string, string> = {}) {
		const validation = new Validator(data, rules, Object.assign(messages, this._messages))

		if (validation.fails()) {
			const errors = validation.errors.all()
			Object.keys(errors).map((key) => {
				errors[key] = { message: errors[key][0] } as any
			})
			return errors
		}
		return null
	}

	public setDefaultMessage(key: string, message: string): void {
		this._messages[key] = message
	}

	public setLangMessages(key: string, messages: any): void {
		Validator.setMessages(key, messages)
	}
}
