export interface IRealTimeProps {
	serverUrl: string
	emitPrepare?: (data: any) => any
}

export type IRealTimeData = Record<string, any>

export class RealTimeService {
	private socket: any

	constructor(private props: IRealTimeProps) {
		this.init()
	}

	private get serverUrl(): string {
		return this.props.serverUrl
	}

	private get ioUrl(): string {
		return this.serverUrl
	}

	private init() {
		if (this.socket) this.socket.close()
		this.socket = new WebSocket(this.ioUrl)
	}

	private prepareData(data: IRealTimeData): IRealTimeData {
		return this.props.emitPrepare ? this.props.emitPrepare(data) : data
	}

	public emit(key: string, data: IRealTimeData = {}) {
		this.socket.emit(key, this.prepareData(data))
	}
}
