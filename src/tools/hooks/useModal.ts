import { useState } from 'react'

export const useModal = <T>(initValue: T) => {
	const [openedModal, changeOpenedModal] = useState<T>(initValue)

	return {
		openedModal,
		openModal: (modal: T) => changeOpenedModal(modal),
		closeModal: () => changeOpenedModal(null),
	}
}
