import { useEffect } from 'react'

export const useMount = (func: () => Promise<any>, setLoading: (value: boolean) => void) => {
	useEffect(() => {
		func().then(() => setLoading(false))
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [])
}
