import { useState } from 'react'

export const useLoading = (initialValue: boolean) => {
	const [isLoading, setLoading] = useState(initialValue)

	const actionWithLoad = async (action: () => Promise<any>) => {
		setLoading(true)
		await action()
		setLoading(false)
	}
	return {
		isLoading,
		setLoading,
		actionWithLoad,
		stopLoading: () => setLoading(false),
		startLoading: () => setLoading(true),
	}
}
