import { useState } from 'react'

interface IUseStepsProps {
	count: number
	initialStep?: number
}

export const useSteps = ({ count = 1, initialStep = 0 }: IUseStepsProps) => {
	const [maxStep, setMaxStep] = useState(count)
	const [step, setStep] = useState(initialStep)

	const changeStep = (_step: number) => {
		if (validStep(_step)) setStep(_step)
	}

	const validStep = (_step: number) => {
		return _step >= 0 && _step < maxStep
	}

	const nextStep = () => {
		if (step + 1 < maxStep) setStep(step + 1)
	}
	const prevStep = () => {
		if (step > 0) setStep(step - 1)
	}

	const reset = () => {
		setStep(0)
	}

	const changeStepsCount = setMaxStep

	return {
		step,
		count: maxStep,
		changeStep,
		nextStep,
		prevStep,
		reset,
		changeStepsCount,
	}
}
