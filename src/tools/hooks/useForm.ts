import { useState } from 'react'

interface Errors {
	[key: string]: string
}

export interface IUseFormState {
	[key: string]: string | number | boolean | any
}
type IValidateMethod = <T>(data: T) => null | Errors

export interface IForm<T> {
	form: T
	setForm: (form: T) => void
	formErrors: { [key: string]: string }
	setFormField: (key: keyof T, value: any) => void
	setFormError: (key: keyof T, error: string) => void
	setFormErrors: (errors: Record<keyof T, string>) => void
	onSubmit: (callback: () => any) => () => any
}

export const useForm = <T extends IUseFormState>(initValue: Partial<T>, validateMethod: IValidateMethod): IForm<T> => {
	const [form, setForm] = useState(initValue as T)
	const [errors, setErrors] = useState({})

	const setFormError = (f: keyof T, e: string = null) => {
		setErrors((oldErrors: any) => {
			return { ...oldErrors, [f]: e }
		})
	}
	const setFormField = (f: keyof T, v: any) => {
		setForm((oldForm: any) => {
			return { ...oldForm, [f]: v }
		})
		setFormError(f, null)
	}

	const validate = () => {
		const _errors = validateMethod(form)
		if (_errors) {
			setErrors(_errors)
			return true
		}
	}

	const onSubmit = (callback: () => any): any => {
		return (...args: any[]): any => {
			if (validate && validate()) {
				return
			}

			callback()
		}
	}

	return {
		form,
		setForm,
		formErrors: errors,
		setFormField,
		setFormError,
		setFormErrors: setErrors,
		onSubmit,
	}
}
