export type IThunk = (dispatch: TDispatch, store?: any) => any

export interface IAction<T = any> {
	type: T
	payload?: Record<string, any>
}

export type TDispatch = (action: IAction) => void
