import { createStore, combineReducers, applyMiddleware, compose, AnyAction, ReducersMapObject } from 'redux'
import { IThunk, IAction } from './redux.interfaces'
import ReactronInit from '../reactron'

export interface IReduxServiceProps<T> {
	reducers: ReducersMapObject<T>
}

interface IStore<T> {
	getState: () => T
	dispatch: (action: IAction) => any
}

export class ReduxService<T> {
	public store: IStore<T>

	constructor(private readonly props: IReduxServiceProps<T>) {
		const rootReducer = combineReducers<T>(props.reducers)
		this.store = createStore(rootReducer, compose(ReactronInit.createEnhancer()))
	}

	private storeDispatchMiddelware(action: IAction) {
		this.store.dispatch({
			type: action.type,
			payload: action.payload,
		})
	}

	public thunkDispatch(thunk: IThunk) {
		return thunk(this.storeDispatchMiddelware, this.store.getState())
	}

	public dispatch = this.storeDispatchMiddelware
}
