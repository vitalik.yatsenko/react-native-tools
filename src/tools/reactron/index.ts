import Reactotron, { asyncStorage } from 'reactotron-react-native'
import { reactotronRedux } from 'reactotron-redux'

const reactron = Reactotron.configure({
	name: 'React Native',
})
	.useReactNative({
		asyncStorage: true,
		networking: {},
		editor: false,
		errors: { veto: (stackFrame: any) => false },
	})
	.use(asyncStorage({}))
	.use(reactotronRedux())
	.connect()

// tslint:disable-next-line: no-console
const yeOldeConsoleLog = console.log

// tslint:disable-next-line: no-console
console.log = (...args: any) => {
	yeOldeConsoleLog(...args)

	Reactotron.display({
		name: 'CONSOLE.LOG',
		important: true,
		value: args,
		preview: args.length ? JSON.stringify(args) : args[0],
	})
}

export default reactron
