export const _shortText = (text: string, limit: number) => {
	if (text.length < limit) return text

	const textArray = text.trim().slice(0, limit).split(' ')

	if (textArray.length > 1) {
		textArray.pop()
		return textArray.join(' ').trim() + '...'
	}
	return textArray.join(' ')
}

export const _capitalizeFirstLetter = (text: string): string => text.charAt(0).toUpperCase() + text.slice(1)

export const _addThreeDots = (text: string, length: number) =>
	text.length > length ? text.substring(0, length - 3) + '...' : text

export const _responseText = (fontSize = 18, koof = 1, text: string) => {
	if (text.length > 10) koof = 2

	fontSize = (180 / Number(text.length)) * koof
	if (fontSize > 20) fontSize = 18
	else if (fontSize < 12) fontSize = 10

	return name
}

export const _anObjectInString = (text: string, object: Record<string, any>): string => {
	Object.keys(object).map((key) => {
		const pattern = new RegExp(`<-${key}->`, 'gm')
		text = text.replace(pattern, object[key])
	})
	return text
}
