export const _safeJsonParse = (json: any): Record<any, any> => {
	if (!json) return null
	return typeof json === 'string' ? JSON.parse(json) : json
}
