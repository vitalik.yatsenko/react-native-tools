export const _getOrDef = <T>(item: T, def: T): T => {
	return item ? item : def
}

export * from './array.helpers'
export * from './json.helpers'
export * from './text.helpers'
