export const _toggleElementInArray = <T>(item: T, arr: T[]): T[] => {
	const result = [...arr]
	const index = result.indexOf(item)

	if (index + 1) result.splice(index, 1)
	else result.push(item)

	return result
}

/*
    params exp: = { name: 'name', years: '19}
  */
export const _createFormData = (params: Record<string, any>, clear: boolean) => {
	const formData = new FormData()

	if (clear) params = _clearClearObj(params)

	Object.keys(params).forEach((key) => {
		const val = params[key]
		if (Array.isArray(val))
			val.map((it, i) => {
				formData.append(key + '[' + i + ']', it)
			})
		else formData.append(key, params[key])
	})

	return formData
}

export const _isNumber = (value: any): boolean => {
	return value && typeof value === 'number' && isFinite(value)
}

export const _isNumberObject = (n: any): boolean => {
	return Object.prototype.toString.apply(n) === '[object Number]'
}

export const _isCustomNumber = (n: any): boolean => {
	return _isNumber(n) || _isNumberObject(n)
}

export const _formatToDecimal = (n: any): number => {
	return +parseFloat(n).toFixed(4)
}

export const _clearClearObj = <T extends Record<string, any>>(obj: T): Partial<T> => {
	return Object.keys(obj).reduce<Partial<T>>((result: any, key: string) => {
		if (obj[key]) result[key] = obj[key]

		return result
	}, {})
}
